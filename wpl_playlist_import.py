
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
import os
import logging
import filecmp

import xml.dom.minidom

import gi
gi.require_version('RB', '3.0')
gi.require_version('PeasGtk', '1.0')
from gi.repository import GObject, Peas, RB, Gio, Gtk

from wpl_playlist_import_convert_to_pls import WplPlaylistImportConvertToPls
# from wpl_playlist_import_convert_to_m3u import WplPlaylistImportConvertToM3u
from wpl_playlist_import_dialog import WplPlaylistImportDialog

class PlaylistWplPlugin(GObject.Object, Peas.Activatable):
    __gtype_name__ = 'PlaylistWplPlugin'
    object = GObject.Property(type=GObject.Object)
    _menu_names = ['playlist-popup']

    def __init__(self):
        GObject.Object.__init__(self)
        self.window = None
        self.dialog = None
        self.action1 = None
        self.plugin_info = "wpl_playlist_import"

    def do_activate(self):
        self.shell = self.object
        app = self.shell.props.application
        self.window = self.shell.props.window
        self.pl_man = self.shell.props.playlist_manager

        self.action1 = Gio.SimpleAction.new("import-wpl-playlists", None)
        self.action1.connect("activate", self.import_playlists, self.object)
        self.window.add_action(self.action1)

        item1 = Gio.MenuItem.new(label="Import Windows Playlist", detailed_action="win.import-wpl-playlists")
        app.add_plugin_menu_item("tools", "import-wpl-playlists", item1)

    def do_deactivate(self):
        self.shell = self.object
        app = self.shell.props.application
        app.remove_plugin_menu_item("view", "import-wpl-playlists")
        app.remove_action("import-wpl-playlists")
        self.action1 = None

    def import_playlists(self, action, parameter, shell):
        dialog = WplPlaylistImportDialog(self)
        dialog.connect("response", self.on_response)
        dialog.show_all()
        dialog.run()
       
    def perform_import(self,wpl_filename):

        wpl_filename_base = os.path.basename(wpl_filename)
        wpl_filename_base_woe = os.path.splitext(wpl_filename_base)[0]
        wpl_filename_base_m3u = wpl_filename_base_woe + ".m3u"

        # Delete any playlist with similar name
        self.import_delete_old_playlist(wpl_filename_base)
        self.import_delete_old_playlist(wpl_filename_base_m3u)

        # Create m3u file from wpl
        # wpl_to_m3u = WplPlaylistImportConvertToM3u(wpl_filename)
        # wpl_filename_tmp = wpl_to_m3u.convert_to_m3u()

        wpl_to_pls = WplPlaylistImportConvertToPls(wpl_filename)
        wpl_filename_tmp = wpl_to_pls.convert_to_pls()

        # Create the name playlist
        pl_uri = "file://" + wpl_filename_tmp
        self.pl_man.parse_file(pl_uri)
        os.remove(wpl_filename_tmp)

    def on_response(self, dialog, response):
        if response == Gtk.ResponseType.OK:
            self.perform_import(dialog.FileSelected)
        dialog.destroy()

    def import_delete_old_playlist(self, playlist_name):
        # # Clean up current playlists
        for playlist in self.pl_man.get_playlists():

        # Delete any named the same as the import
            if(playlist.props.name == playlist_name): 
                result = self.pl_man.delete_playlist(playlist.props.name)