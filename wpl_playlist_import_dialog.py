
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version('RB', '3.0')
gi.require_version('PeasGtk', '1.0')
from gi.repository import Gtk

class WplPlaylistImportDialog(Gtk.Dialog):

    def __init__(self, parent):
        Gtk.Dialog.__init__(self)
        self.set_title("Import Windows Play List")
        self.add_button("_OK", Gtk.ResponseType.OK)
        self.add_button("_Cancel", Gtk.ResponseType.CANCEL)
        self.FileSelected = None

        fileFilter = Gtk.FileFilter()
        fileFilter.add_pattern("*.wpl")

        box = Gtk.Box()
        self.FileChooserLabel = Gtk.Label(label="Windows Play List")
        self.FileChooserButton = Gtk.FileChooserButton(title="Browse")
        self.FileChooserButton.set_local_only(True)
        self.FileChooserButton.add_filter (fileFilter)
        self.FileChooserButton.set_create_folders(False)
        box.add(self.FileChooserLabel)
        box.add(self.FileChooserButton)

        self.set_default_size(250, 100)
        
        self.FileChooserButton.connect("file-set", self.file_changed)    

        self.vbox.add(box)

    def file_changed(self,filechooserbutton):
        self.FileSelected = filechooserbutton.get_filename()
