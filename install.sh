#!/bin/bash

#Also thanks to David Mohammed. This code is taken from the coverart-browser plugin

################################ USAGE #######################################

usage=$(
cat <<EOF
Usage:
$0 [OPTION]
-h, --help      show this message.

EOF
)

########################### OPTIONS PARSING #################################

#parse options
TMP=`getopt --name=$0 -a --longoptions=help -o h -- $@`

if [[ $? == 1 ]]
then
    echo
    echo "$usage"
    exit
fi

eval set -- $TMP

until [[ $1 == -- ]]; do
    case $1 in
        -h|--help)
            echo "$usage"
            exit
            ;;
    esac
    shift # move the arg list to the next option or '--'
done
shift # remove the '--', now $1 positioned at first argument if any

########################## START INSTALLATION ################################

SCRIPT_NAME=`basename "$0"`
SCRIPT_PATH=${0%`basename "$0"`}
PLUGIN_PATH="${HOME}/.local/share/rhythmbox/plugins/wpl_playlist_import/"
GLIB_SCHEME="org.gnome.rhythmbox.plugins.wpl_playlist_import.gschema.xml"
GLIB_DIR="/usr/share/glib-2.0/schemas/"

#build the dirs
mkdir -p $PLUGIN_PATH

#copy the files
cp -r "${SCRIPT_PATH}"* "$PLUGIN_PATH"

#remove the install script from the dir (not needed)
rm "${PLUGIN_PATH}${SCRIPT_NAME}"

#install the glib schema
if [ ! -f $GLIB_DIR$GLIB_SCHEME ]; then
    echo "Installing the glib schema (password needed)" && sudo cp "${PLUGIN_PATH}${GLIB_SCHEME}" "$GLIB_DIR" && sudo glib-compile-schemas "$GLIB_DIR"
fi

read -p "Script execution ended, press [Enter] key to exit..."
