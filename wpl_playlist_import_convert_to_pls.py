#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import tempfile

import xml.dom.minidom

from shutil import copyfile

class WplPlaylistImportConvertToPls:

    def __init__(self,wpl_filename):
        self._wpl_filename = wpl_filename

    def convert_to_pls(self):

        # Get the base name of the file to be converted
        wpl_filename_base = os.path.basename(self._wpl_filename).replace(' ','_')
        wpl_filename_base_woe = os.path.splitext(wpl_filename_base)[0]
        wpl_filename_base_pls = wpl_filename_base_woe + ".pls"

        # Set name of output file to use
        try: 
            temp_file_handle = tempfile.NamedTemporaryFile()
#            print('Name of temporary filename to use for pls conversion: ' + temp_file_handle.name)

            ext3tag = '[playlist]'
            temp_file_handle.write(ext3tag.encode())
            temp_file_handle.write('\n'.encode())

            playlistTag = 'X-GNOME-Title=' + wpl_filename_base_woe
            temp_file_handle.write(playlistTag.encode())
            temp_file_handle.write('\n'.encode())

            self.write_pls_entries(temp_file_handle)
            
            temp_file_handle.flush()

            # E.g. /tmp/playlist.pls
            wpl_filename_tmp = os.path.join(tempfile.gettempdir(),"00." + wpl_filename_base_pls)

            # Copy file to /tmp/playlist.pls
            copyfile(temp_file_handle.name, wpl_filename_tmp)
            temp_file_handle.close()

        finally:
            temp_file_handle.close()
        
        return wpl_filename_tmp

    def write_pls_entries(self,temp_file_handle):

        # Open as xml file
        doc = xml.dom.minidom.parse(self._wpl_filename)

        # Get the name of the files based of the tag media
        media_files = doc.getElementsByTagName("media")

        numberOfEntries = 'NumberOfEntries=' + str(media_files.length) 
        temp_file_handle.write(numberOfEntries.encode())
        temp_file_handle.write('\n'.encode())

        # Iterate of the the media tags
        counter = 1
        for media_file in media_files:
            src_filename = media_file.getAttribute('src')
            self.write_pls_entry(temp_file_handle,src_filename, counter)
            counter = counter + 1

    def write_pls_entry(self, temp_file_handle, src_filename, counter):
        numberOfEntries = 'File' + str(counter) + "="
        temp_file_handle.write(numberOfEntries.encode())

        new_filename = self.get_pls_filename(src_filename.encode())
        temp_file_handle.write(new_filename)
        temp_file_handle.write('\n'.encode())

    def get_pls_filename(self, src_filename):
        #samba_name_forward_slash = self._samba_name.replace("\\".encode(), "/".encode())
        #src_filename_forward_slash = src_filename.replace("\\".encode(), "/".encode())
        #new_filename = src_filename_forward_slash.replace(samba_name_forward_slash, self._cifs_name)
        new_filename_with_samba = src_filename.replace("\\\\".encode(), "smb://".encode())
        new_filename = new_filename_with_samba.replace("\\".encode(), "/".encode())
        return new_filename

    @staticmethod
    def print_pls_file(temp_file_handle_name):
        temp_file_handle = open(temp_file_handle_name,"r")
        temp_file_handle.close()