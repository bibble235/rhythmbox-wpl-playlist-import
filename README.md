Playlists import/export
==========================
A Rhythmbox plugin to import a play list from Windows Samba share. 

Some details:
- Automatic playlists are not affected (only static ones)

Installation
--------------------
- Pull the project and run the install.sh .
- Then restart Rhythmbox

Usage
--------------------
- In the RB menu click Plugins, select the plugin, click Preferences and choose the directory you want to import from or export to. (the default one is /home/user/playlists_import_export)
- Use the 'Import/Export playlists' buttons in the Tools menu

Change log
--------------------
- 1.0.0 Initial release
